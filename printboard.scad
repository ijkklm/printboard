/*
 *
 *  printboard - A 3D-printable prototyping board for electronic circuits.
 *
 *  Copyright (C) 2020 Frank Hermann (frank_dot_c_dot_hermann_at_googlemail_dot_com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


//slicer related settings
eps = 0.001;
perimeter_width = 0.25 + eps;
layer_height = 0.1;
first_layer_height = 0.2;

//general settings
$fn = 64;
hole_cutout_size = 0.6;
hole_cutout_wall_width = perimeter_width;
insertion_width_typical = 0.3; //typical diameter of inserted wires. smaller should work better for DIP packages
insertion_width_max = 0.7;
contact_shift_y = insertion_width_typical/2;
hole_dist = 2.54; //distance between holes of the breadboard
hole_size = 1.1*insertion_width_max;
rod_d = 0.75 /*1.0 + 0.1*/; //diameter of the rod which connects the pins of a row
contact_point_dist = 3.2; //distance from surface to the contact point (where the clip presses the inserted wire against the rod)
top_thickness = first_layer_height + layer_height;
//total height of the clip (critical parameter: if the clip is to long
//some wires/pins which are inserted will not reach the contact point, if it is to short it will break off easily)
clip_height = contact_point_dist - top_thickness;
clip_thickness = perimeter_width;
clip_tip_height = 2*layer_height; //specifies the length of the vertical section of the clip at the contact point (to increase the friction)
clip_guide_dist = 0.3;
clip_x_dist = 0.4; //critical parameter: to small and adjacent clips will be "melted together" due to stringing, to large and contact pressure will decrease (because the clip width decreases)
clip_x = hole_dist - clip_x_dist; //width of a clip
seperator_width = 2*perimeter_width; //thickness of the wall between the top of the board and the rod
seperator_height = clip_height - 0.2*rod_d;
wall_height = top_thickness + 4.4; //controls the height of the board
wall_x_width = 4*perimeter_width; //width of outer x-wall
wall_y_width = 4*perimeter_width; //width of outer y-wall
enable_short_protector = true;
short_protector_width = perimeter_width;
short_protector_top_width = 0; //set to 0 to simplify soldering. however, the short protector will then only serve as a limit stop
limit_stop_distance = insertion_width_max; //only available when enable_short_protector is enbaled; set to zero to disable limit stop
short_protector_slope = 45; //relative to horizon, should be 45 degrees or more

//settings for double sided boards
center_cutout_x = 0; //set to 0 for automatic selection
center_cutout_z = 1.5; //set to 0 to disable center cutout
rod_cutout_width = rod_d; //width of the observation slit
rod_depth = 2.2; //penetration depth of rod at the center of the board
rod_hole_d = 0.9*rod_d; //percent of rod diameter
rod_slit_dist = perimeter_width;
rod_slit_width = 0.15;
rod_slit_height = 1.5*rod_d;

//multi clip settings (currently not usable)
multi_clip_cnt = 0;
clip_joint_height = layer_height;
clip_distance = clip_guide_dist;

/*
 * generates a cube which can be slanted at two sides
 */
module cube2(x, y, z, tx, ty){
	vertices = [
		[ 0,  0, 0],
		[ x,  0, 0],
		[ x,  y, 0],
		[ 0,  y, 0],
		[ 0,  0, z],
		[tx,  0, z],
		[tx, ty, z],
		[ 0, ty, z]];
	faces = [
		[0,1,2,3],
		[4,5,1,0],
		[7,6,5,4],
		[5,6,2,1],
		[6,7,3,2],
		[7,4,0,3]];
	polyhedron(vertices, faces);
}

/*
 * generates a cube which can be slanted at all four sides
 */
module cube3(x, y, z, tx1, ty1, tx2, ty2){
	vertices = [
		[ 0,   0,  0],
		[ x,   0,  0],
		[ x,   y,  0],
		[ 0,   y,  0],
		[tx2, ty1, z],
		[tx2, ty2, z],
		[tx1, ty2, z],
		[tx1, ty1, z]];
	faces = [
		[0,1,2,3],
		[4,5,1,0],
		[7,6,5,4],
		[5,6,2,1],
		[6,7,3,2],
		[7,4,0,3]];
	polyhedron(vertices, faces);
}

/*
 * Generates a special object (see below) which protects the pin from an electrical short with another pin in the next row.
 * A limit stop for the clip is included if enabled.
 */
module short_protector(last){
	/*
	 ay = additional_y
	 p = short_protector_slope (an angle)

	      |-ay-|-y0-|
	 *    +----+--------  *     --|  --|  --|
	 *    |    |\         *       |    |    |
	  *   |    |p\       *        z0   |    |
	   *  |    |  \     *         |    |    |
	    * |    |   \  *           |    |    |
	      |*   |   /*           --|    |    |
	      |   *|* /                    z1   |
	      |    | /                     |    |
	      |    |/                    --|    z2
	      |    /                            |
	      |   /                             |
	      |  /                              |
	      | /                               |
	      |/                              --|

	*/
	size_x = 2*hole_dist - clip_x;
	r = rod_d/2 + short_protector_width;
	additional_y = max(0, rod_d/2 + contact_shift_y - (hole_size/2 + seperator_width));
	y0 = r*sin(short_protector_slope);
	y1 = y0 + additional_y;
	z0 = r*cos(short_protector_slope);
	z1 = r/cos(short_protector_slope);
	z2 = additional_y*(z1 - z0)/y0 + z1;
	h0 = clip_tip_height/2;
	difference(){
		union(){
			intersection(){
				cube([r, r, size_x]);
				cylinder(h=size_x, r=r);
			}
			translate([0, -additional_y, size_x]) rotate([-90, 0, 0]) cube2(z2, size_x, y1, z0, size_x);
			if(limit_stop_distance > 0){
				w = hole_dist - clip_thickness - limit_stop_distance - rod_d/2 + additional_y;
				h1 = h0 + w*tan(short_protector_slope);
				translate([0, -additional_y, size_x]) rotate([-90, 0, 0]) cube2(h1, size_x, w, h0, size_x);
			}
		}
		if(!last){
			cylinder(h=size_x, d=rod_d);
		}
	}
	if(limit_stop_distance > 0){
		stopper_w = hole_dist - clip_thickness - limit_stop_distance - rod_d - short_protector_top_width;
		translate([-clip_tip_height/2, rod_d/2 + short_protector_top_width, 0]) cube([clip_tip_height/2, stopper_w, size_x]);
	}
	if(!last){
		translate([contact_point_dist - wall_height, rod_d/2, 0]) cube([wall_height - contact_point_dist, short_protector_top_width, size_x]);
	}else{
		translate([contact_point_dist - wall_height, -additional_y, 0]) cube([wall_height - contact_point_dist, rod_d/2 + short_protector_top_width + additional_y, size_x]);
	}
}

/*
 * Generates the clip which presses the inserted wire against the rod.
 */
module clip(){
	clip_displace_y = hole_size/2 + contact_shift_y + hole_cutout_size + hole_cutout_wall_width;
	h = clip_height - clip_tip_height/2;
	s = clip_displace_y/h;
	dx = hole_size/2;
	m_x = -clip_x/2;
	m_y = hole_size/2 - clip_displace_y + hole_cutout_size + hole_cutout_wall_width;
	m_z = -h;
	multmatrix(m=[[1, 0,  0,  m_x],
	              [0, 1,  s,  m_y],
	              [0, 0,  1,  m_z],
	              [0, 0,  0,    1]]) cube([clip_x, clip_thickness, h]);
	translate([m_x, m_y, -clip_height - clip_tip_height/2]) cube([clip_x, clip_thickness, clip_tip_height]);
}

/*
 * Generates the shape of the cutout for the top side of the board.
 * This makes insertion of wires easier and allows DIP packages to be inserted deeper.
 */
module pin_cutout(){
	s = 2*hole_cutout_size + hole_size;
	translate([-s/2, -s/2, top_thickness]) mirror([0, 0, 1]) cube3(s, s, hole_cutout_size, hole_cutout_size, hole_cutout_size, hole_cutout_size + hole_size, hole_cutout_size + hole_size);
}

/*
 * Generates a single pin with clip and optional short protection.
 * The boolean arguments indicates whether the pin is in the last row of the board or not.
 */
module pin(last){
	inner_hole_dist = 2*hole_dist - hole_size;
	size_x = 2*hole_dist - clip_x;
	lower_y = hole_dist - hole_size - hole_cutout_size - hole_cutout_wall_width + clip_guide_dist;
	upper_y = hole_dist - hole_size/2 + contact_shift_y + clip_guide_dist;
	height = clip_height - clip_tip_height/2;
	difference(){
		union(){
			translate([0, (hole_dist - hole_size - seperator_width)/2, -seperator_height/2]) cube([size_x, hole_dist + seperator_width, seperator_height], center=true);
			translate([0, 0, top_thickness/2]) cube([size_x, inner_hole_dist, top_thickness], center=true);
		}
		translate([-size_x/2, -rod_d/2 - contact_shift_y, -clip_height]) rotate([0, 90, 0]) cylinder(d=rod_d, h=size_x);
		translate([0, 0, top_thickness - contact_point_dist/2]) cube([hole_size, hole_size, contact_point_dist], center=true);
		union(){
			translate([-size_x/2, hole_size/2 - clip_guide_dist + hole_cutout_wall_width + lower_y + hole_cutout_size]) rotate([180, 0, 0]) cube2(size_x, lower_y, height, size_x, upper_y);
			translate([-size_x/2, -contact_shift_y - clip_guide_dist, -clip_height]) cube([size_x, upper_y, clip_tip_height/2]);
		}
		pin_cutout();
	}
	clip();
	if(enable_short_protector){
		translate([-size_x/2, -rod_d/2 - contact_shift_y + hole_dist, -clip_height]) rotate([0, -90, 180]) short_protector(last);
	}
	if(multi_clip_cnt > 0){
		for(x = [1:1:multi_clip_cnt]){
			translate([0, x*(clip_distance + clip_thickness), 0]) clip();
		}
		translate([-clip_x/2, -clip_thickness/2, -clip_joint_height/2 - clip_height]) cube([clip_x, multi_clip_cnt*(clip_distance + clip_thickness), clip_joint_height]);
	}
}

module wall_x(count_x, count_y){
	difference(){
		clip_dist = hole_dist - clip_x;
		translate([-wall_x_width - clip_x/2 - clip_dist, -hole_dist + hole_size/2, -wall_height + top_thickness]) cube([wall_x_width, count_y*hole_dist - hole_dist + 2*(hole_dist - hole_size/2), wall_height]);
		for(y = [0:count_y-1]){
			translate([-clip_x/2 - clip_dist - wall_x_width, -rod_d/2 + y*hole_dist - contact_shift_y, -clip_height]) rotate([0, 90, 0]) cylinder(d=rod_d, h=count_x*hole_dist + 2*wall_x_width + clip_dist + clip_x);
		}
	}
}

module wall_y(count_x, count_y, inner_width){
	clip_dist = hole_dist - clip_x;
	translate([-clip_dist - clip_x/2 - wall_x_width, hole_size/2 - wall_y_width - hole_dist , -wall_height + top_thickness]) cube([2*(wall_x_width + clip_dist) + clip_x + (count_x - 1)*hole_dist, wall_y_width + inner_width, wall_height]);
}

/*
 * Generates a Breadboard of the given pin count.
 * The argument count_x specifies the pin count of each row and
 * count_y specifies the row count.
 */
module single_board(count_x, count_y){
	difference(){
		union(){
			for(x = [0:count_x-1]){
				for(y = [0:count_y-1]){
					translate([hole_dist*x, hole_dist*y, 0]) pin(y == count_y - 1);
				}
			}
		}
		for(x = [0:count_x-1]){
			for(y = [0:count_y-1]){
				translate([hole_dist*x, hole_dist*y, 0]) pin_cutout();
			}
		}
	}
	wall_x(count_x, count_y);
	translate([hole_dist*(count_x - 1), 0, 0]) mirror([1, 0, 0]) wall_x(count_x, count_y);
	wall_y(count_x, count_y, 0);
	translate([0, (count_y - 1)*hole_dist, 0]) mirror([0, 1, 0]) wall_y(count_x, count_y, seperator_width);
}

module center_rod_cutouts(count_x, ypos){
	required_rod_length = wall_x_width + count_x*hole_dist + clip_x_dist;
	echo("required rod length: ", required_rod_length + rod_depth);
	x = clip_x_dist + clip_x/2;
	y = hole_dist*ypos - rod_d/2 - contact_shift_y;
	translate([x - eps, y, -clip_height]) rotate([0, 90, 0]) scale([1, rod_hole_d/rod_d, 1]) cylinder(d=rod_d, h=rod_depth + eps);
	translate([x + rod_depth - rod_cutout_width, y - rod_hole_d/2, -clip_height - wall_height]) cube([rod_cutout_width, rod_hole_d, wall_height]);
	translate([x - eps, y + rod_hole_d/2 + rod_slit_dist, -clip_height -rod_slit_height/2]) cube([rod_depth + eps, rod_slit_width, rod_slit_height]);
	translate([x - eps, y - rod_hole_d/2 - rod_slit_dist - rod_slit_width, -clip_height -rod_slit_height/2]) cube([rod_depth + eps, rod_slit_width, rod_slit_height]);
}

/*
 * Generates a "typical" breadboard with count_y rows.
 * The pin count of the rows of the left half is specified by count_x1,
 * while count_x2 specifies the pin count of each row on the right half.
 * The distance (in pins) between the halfs is given by count_x_spacer (usually equals 2).
 */
module double_sided_board(count_x1, count_x2, count_y, count_x_spacer){
	difference(){
		clip_dist = hole_dist - clip_x;
		thickness = (count_x_spacer + 1)*hole_dist - 2*(clip_dist + clip_x/2);
		l = count_y*hole_dist - hole_dist + 2*(hole_dist - hole_size/2) + 2*wall_y_width;
		m_y = hole_size/2 - hole_dist - wall_y_width;
		m_x = (count_x1 + (count_x_spacer - 1)/2)*hole_dist;
		union(){
			single_board(count_x1, count_y);
			translate([(count_x_spacer + count_x1)*hole_dist, 0, 0]) single_board(count_x2, count_y);
			translate([m_x - thickness/2, m_y, -wall_height + top_thickness]) if(center_cutout_x != 0){
				cube([thickness, l, wall_height]);
			}else{
				cube([thickness, l, wall_height - center_cutout_z]);
			}
		}
		if(center_cutout_x != 0){
			translate([m_x - center_cutout_x/2, m_y, top_thickness - center_cutout_z]) cube([center_cutout_x, l, center_cutout_z]);
		}
		for(y = [0:count_y-1]){
			translate([hole_dist*(count_x1 - 1), 0, 0]) center_rod_cutouts(count_x1, y);
			translate([hole_dist*(count_x1 + count_x_spacer), 0, 0]) mirror([1, 0, 0]) center_rod_cutouts(count_x2, y);
		}
	}
}

/*
 * Special helper for boards generated by double_sided_board.
 * It outputs a cube exactly where the bridge between the two halfs of the Breadboard needs to be printed.
 * This allows reduction of the perimeter count so that the bridge is correctly "anchored".
 * The object needs to be generated and exported in a seperate .stl.
 */
module double_sided_board_center_mod(count_x1, count_x2, count_y, count_x_spacer){
	clip_dist = hole_dist - clip_x;
	thickness = (count_x_spacer + 1)*hole_dist - 2*(wall_x_width + clip_dist + clip_x/2);
	l = count_y*hole_dist - hole_dist + 2*(hole_dist - hole_size/2) + 2*wall_y_width;
	m_y = hole_size/2 - hole_dist - wall_y_width;
	m_x = (count_x1 + (count_x_spacer - 1)/2)*hole_dist;
	depth = 2*perimeter_width;
	height = layer_height;
	length_reduction = 4*perimeter_width;
	translate([m_x - thickness/2 - depth, m_y + length_reduction/2, top_thickness - height - center_cutout_z]) cube([thickness + 2*depth, l - length_reduction, height]);
}

mirror([0, 0, 1]) difference(){
	//single_board(2, 2);
	double_sided_board(6, 6, 15, 2);
	//double_sided_board(6, 6, 2, 2);
	//uncomment the following lines to inspect a cross section
	s = 100;
	//translate([-s/2, 0, 0]) cube([s, s, s], center=true);
	//translate([0, 0, -s/2 - clip_height - rod_d/4]) cube([s, s, s], center=true);
}

//uncomment the following line to generate the modifier volume for the slicer
//mirror([0, 0, 1]) double_sided_board_center_mod(6, 6, 15, 2);
